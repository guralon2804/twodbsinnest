import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const MongoConfig: TypeOrmModuleOptions = {
    type: 'mongodb',
    host: 'localhost',
    port: 27017,
    //username: process.env.DB_USERNAME || '', 
    //password: process.env.DB_PWD || '',    
    database: 'Corona',    
    entities: [__dirname + '/../**/*.entity.js'],
    synchronize: true,
    useNewUrlParser: true,
    name:'diagnoserConnection'
}

export const secondMongoConfig: TypeOrmModuleOptions = {
    type: 'mongodb',
    host: 'localhost',
    port: 27017,
    //username: process.env.DB_USERNAME || '', 
    //password: process.env.DB_PWD || '',    
    database: 'test',    
    entities: [__dirname + '/../**/*.entity.js'],
    synchronize: true,
    useNewUrlParser: true,
    name:'paientConnection'
}