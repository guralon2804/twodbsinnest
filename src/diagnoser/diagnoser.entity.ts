import { BaseEntity, Entity, Column, PrimaryGeneratedColumn, ObjectID, ObjectIdColumn } from 'typeorm'

@Entity()
export class Diagnoser extends BaseEntity {

    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    FirstName: string;

    @Column()
    LastName: string;
}