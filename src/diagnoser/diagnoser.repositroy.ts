import { Repository, EntityRepository } from "typeorm";
import { Diagnoser } from "./diagnoser.entity";
import { newPersonDto } from "./dto/newPerson.dto";

@EntityRepository(Diagnoser)
export class DiagnoserRepository extends Repository<Diagnoser>{
    async createDiagnoser(newPerson: newPersonDto) {
        const diagnoser = new Diagnoser();        
        diagnoser.FirstName = newPerson.FirstName;
        diagnoser.LastName = newPerson.LastName;
        const result = await diagnoser.save();
        return result;
    }
}