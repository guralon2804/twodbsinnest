import { Controller, Post, Body, Get, Param, Patch, Delete } from '@nestjs/common';
import { newPersonDto } from './dto/newPerson.dto';
import { DiagnoserService } from './diagnoser.service';
import { PaientService } from 'src/paient/paient.service';

@Controller()
export class DiagnoserController {
    constructor(private readonly diagnoserService: DiagnoserService, 
        private readonly paientService: PaientService) { }

    @Post()
    async createDiagnoser(@Body() newPerson: newPersonDto) {        
        const result  = await this.diagnoserService.createDiagnoser(newPerson);
        return result;
    }

    @Get()
    async getAllDiagnoser(){
        const result  = await this.diagnoserService.getDiagnoser();
        result.forEach(async x=> {
            await this.paientService.createDiagnoser({FirstName:x.FirstName, LastName: x.LastName});
        })
        return result;
    }

    @Get(':id')
    async getDiagnoserById(@Param('id') id: string){

        const result  = await this.diagnoserService.getDiagnoserById(id);
        return result;
    }

    @Patch(':id')
    async updateDiagnoser(@Body() updatePerson: newPersonDto, @Param('id') id: string){
        const result  = await this.diagnoserService.updateDiagnoser(updatePerson, id);
        return result;
    }

    @Delete(':id')
    async deleteDiagnoser(@Param('id') id: string){
        const result = await this.diagnoserService.deleteDiagnoser(id);
        return result;
    }
}
