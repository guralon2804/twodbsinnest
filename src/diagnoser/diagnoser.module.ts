import { Module } from '@nestjs/common';
import { DiagnoserController } from './diagnoser.controller';
import { DiagnoserService } from './diagnoser.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { DiagnoserRepository } from './diagnoser.repositroy';
import { PaientService } from 'src/paient/paient.service';
import { PaientRepository } from 'src/paient/paient.repositroy';

@Module({
  imports: [TypeOrmModule.forFeature([DiagnoserRepository],'diagnoserConnection')],
  controllers: [DiagnoserController],
  providers: [DiagnoserService,DiagnoserRepository, PaientService, PaientRepository]
})
export class DiagnoserModule {}
