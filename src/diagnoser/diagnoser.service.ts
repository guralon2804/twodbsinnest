import { Injectable, NotFoundException } from '@nestjs/common';
import { DiagnoserRepository } from './diagnoser.repositroy';
import { newPersonDto } from './dto/newPerson.dto';
import { Diagnoser } from './diagnoser.entity';
import { InjectRepository, InjectConnection, InjectEntityManager } from '@nestjs/typeorm';
import { Connection, EntityManager } from 'typeorm';

@Injectable()
export class DiagnoserService {
    
    constructor(
        @InjectRepository(DiagnoserRepository)
        private readonly diagnoserRepository: DiagnoserRepository,
        @InjectConnection('diagnoserConnection')
        private connection: Connection,
        @InjectEntityManager('diagnoserConnection')
        private entityManager: EntityManager) { }

    async createDiagnoser(newDiagnoser: newPersonDto) {
        try {            
            const result = await this.diagnoserRepository.createDiagnoser(newDiagnoser);
            return result;
        } catch (err) {
            throw new NotFoundException();
        }
    }

    async getDiagnoser() {
        const result =await this.entityManager.find(Diagnoser);
        //const result = await Diagnoser.find();
        return result;
    }

    async getDiagnoserById(id: string) {
        const result = await Diagnoser.findOne(id);
        if(!result){
            throw new NotFoundException()
        }
        return result;
    }

    async updateDiagnoser(diagnoser: newPersonDto, _id: string) {
        const result = await this.getDiagnoserById(_id);
        result.FirstName = diagnoser.FirstName || result.FirstName;
        result.LastName = diagnoser.LastName || result.LastName;
        await result.save();
        return result;
    }

    async deleteDiagnoser(id){
        const result = await this.diagnoserRepository.delete(id);
        console.log(result);
    }
}
