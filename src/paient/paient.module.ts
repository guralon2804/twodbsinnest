import { Module } from '@nestjs/common';
import { PaientController } from './paient.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Paient } from './paient.entity';
import { PaientService } from './paient.service';
import { PaientRepository } from './paient.repositroy';

@Module({
  imports:[TypeOrmModule.forFeature([Paient], 'paientConnection')],
  controllers: [PaientController],
  providers: [PaientService, PaientRepository]
})
export class PaientModule {}
