import { Controller, Get } from '@nestjs/common';
import { PaientService } from './paient.service';

@Controller('paient')
export class PaientController {
    constructor(
        private readonly paientService: PaientService) { }
    @Get()
    async getAllDiagnoser(){
        console.log('ss');
        const result  = await this.paientService.getDiagnoser();       
        return result;
    }
}
    