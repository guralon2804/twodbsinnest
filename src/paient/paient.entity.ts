import { BaseEntity, Entity, Column, ObjectID, ObjectIdColumn } from 'typeorm'

@Entity()
export class Paient extends BaseEntity {

    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    FirstName: string;

    @Column()
    LastName: string;
}