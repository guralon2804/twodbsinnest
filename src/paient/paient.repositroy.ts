import { Repository, EntityRepository } from "typeorm";
import { newPersonDto } from "src/diagnoser/dto/newPerson.dto";
import { Paient } from "./paient.entity";

@EntityRepository(Paient)
export class PaientRepository extends Repository<Paient>{
    async createDiagnoser(newPerson: newPersonDto) {
        const diagnoser = new Paient();        
        diagnoser.FirstName = newPerson.FirstName;
        diagnoser.LastName = newPerson.LastName;
        const result = await diagnoser.save();        
        return result;
    }
}