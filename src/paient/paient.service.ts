import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository, InjectConnection, InjectEntityManager } from '@nestjs/typeorm';
import { PaientRepository } from './paient.repositroy';
import { newPersonDto } from 'src/diagnoser/dto/newPerson.dto';
import { Connection, EntityManager } from 'typeorm';
import { Paient } from './paient.entity';

@Injectable()
export class PaientService {
    constructor(
        @InjectRepository(PaientRepository)
        private readonly diagnoserRepository: PaientRepository,
        @InjectConnection('paientConnection')
        private connection: Connection,
        @InjectEntityManager('paientConnection')
        private entityManager: EntityManager, ) { }

    async createDiagnoser(newDiagnoser: newPersonDto) {
        try {
            const diagnoser = this.entityManager.create(Paient);
            diagnoser.FirstName = newDiagnoser.FirstName;
            diagnoser.LastName = newDiagnoser.LastName;
            const result = await this.entityManager.save(diagnoser);
            console.log(result);
            return result;
        } catch (err) {
            console.log(err);
        }
    }
    async getDiagnoser() {
        console.log("hi");
        const result = await Paient.find();
        return result;
    }

}
