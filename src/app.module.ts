import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { MongoConfig, secondMongoConfig } from './config/mssql';
import { DiagnoserModule } from './diagnoser/diagnoser.module';
import { PaientModule } from './paient/paient.module';
import { Diagnoser } from './diagnoser/diagnoser.entity';
import { Paient } from './paient/paient.entity';

@Module({
  imports: [
    DiagnoserModule,
    TypeOrmModule.forRoot({...MongoConfig}), 
    PaientModule,
     TypeOrmModule.forRoot({...secondMongoConfig})],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
